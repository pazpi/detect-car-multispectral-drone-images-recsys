import matplotlib.pyplot as plt
from pony import orm
from pony_db import Frame

with orm.db_session:
    blur = orm.select(f.laplacian_blur for f in Frame)[:]
# the histogram of the data
n, bins, patches = plt.hist(blur, 50, normed=1, facecolor='green', alpha=0.75)

plt.xlabel('Laplacian Blur')
plt.ylabel('Number of photos')
plt.title(r'$\mathrm{Histogram}$')
# plt.axis([0, 1600, 0, 0.03])
plt.grid(True)

plt.show()
