import os
import cv2
from pony_db import Frame
from pony import orm


list_video = ['Multisensor-Visible.avi', 'Multisensor-Thermal.avi']


def variance_of_laplacian(image):
    # compute the Laplacian of the image and then return the focus
    # measure, which is simply the variance of the Laplacian
    return cv2.Laplacian(image, cv2.CV_64F).var()


video = cv2.VideoCapture(list_video[0])

n_frame = 1
frame_blur = {}
while video.isOpened():
    ret, frame = video.read()
    if ret:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        blur = variance_of_laplacian(frame)
        print('Frame: {0:4d}, blur: {1}'.format(n_frame, blur))
        frame_blur[n_frame] = blur
        n_frame = n_frame + 1
    else:
        break


with orm.db_session:
    for key in frame_blur.keys():
        Frame(frame=key, laplacian_blur=frame_blur[key])
