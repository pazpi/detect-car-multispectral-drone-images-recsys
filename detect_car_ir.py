import os
import cv2
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.metrics import classification_report, confusion_matrix

import enum
from typing import List, Dict


class SVMLabels(enum.Enum):
    other = 0
    hot_engine = 1


plt.rcParams['figure.figsize'] = [18, 16]

fram_ir_dir = './frame-ir'

AxisVector = List[plt.Axes]
SVMLabel_type = List[int]

std_threshold = 10.0

svm_train_data = []
svm_train_labels = []


def load_images():
    pos_img = []
    neg_img = []
    for root, dirs, files in os.walk(fram_ir_dir, topdown=True):
        for name in files:
            if root == os.path.join(fram_ir_dir, 'positive'):
                pos_img.append(os.path.join(root, name))
            if root == os.path.join(fram_ir_dir, 'negative'):
                neg_img.append(os.path.join(root, name))

    pos_img.sort()
    neg_img.sort()
    return pos_img, neg_img


def read_img(img_name: str):
    img = cv2.imread(img_name, cv2.IMREAD_GRAYSCALE)
    return img


def equalize_img(img: np.ndarray):
    return cv2.equalizeHist(img)


def CLAHE_equalization(img: np.ndarray):
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
    return clahe.apply(img)


def shrink_image(img: np.ndarray, step: int = 15):
    col_idx = [col for col in range(0, np.shape(img)[1], step)]
    img_shrink = img[:, col_idx]
    return img_shrink


def remove_still_img_column(img: np.ndarray):
    col_above_threshold = [img[:, id_shrink] for id_shrink in range(img.shape[1]) if
                           np.std(img[:, id_shrink]) > std_threshold]

    img_clean = np.asarray(col_above_threshold)
    img_clean = np.swapaxes(img_clean, 0, 1)
    return img_clean


def split_image(img: np.ndarray, ratio: float, offset: float):
    num_row = img.shape[0]
    low_limit = int(np.floor(offset * num_row))
    up_limit = int(np.floor((ratio + offset) * num_row))
    if low_limit > up_limit:
        low_limit, up_limit = up_limit, low_limit
    return img[low_limit:up_limit, :]


def plot_img_and_hist(img: np.ndarray,
                      img_array_hist: np.ndarray,
                      fig: plt.Figure,
                      axes_img: plt.Axes,
                      axes_hist: plt.Axes) -> (np.ndarray, np.ndarray):
    axes_img.imshow(img)
    n, bins, _ = axes_hist.hist(img_array_hist, bins=10, range=(150, 255), color='#95d0fc')

    bin_centers = 0.5 * np.diff(bins) + bins[:-1]
    axes_hist.axvline(x=np.average(bin_centers, weights=n), linewidth=8, color='blue')

    for count, x in zip(n, bin_centers):
        # Label the raw counts
        axes_hist.annotate(str(count), xy=(x, 0), xycoords=('data', 'axes fraction'),
                           xytext=(0, 68), textcoords='offset points', va='top', ha='center')

    return n, bins, bin_centers


def processing_image(img_path: str,
                     img_label: SVMLabels,
                     fig: plt.Figure,
                     axis: AxisVector) -> (np.ndarray, np.ndarray):
    img_name = img_path[-7:-4]
    img = read_img(img_path)
    img = equalize_img(img)
    img_shrink = shrink_image(img)
    img_shrink_clean = remove_still_img_column(img_shrink)

    template_str = "IMG: {0} {1}\tvalue {2:.1f}\tat bin {3} ({4})\tmean {5:.2f}\tLabel {6}"

    # UP Section
    up_img_split = split_image(img, 0.3, 0)
    up_shrink_img = split_image(img_shrink_clean, 0.3, 0)
    up_shrink_array = np.asarray(up_shrink_img).ravel()

    n, bins, bin_centers = plot_img_and_hist(up_img_split, up_shrink_array, fig, axis[0], axis[1])
    print(template_str.format(img_name,
                              'UP',
                              np.max(n),
                              n.argmax(),
                              bins[n.argmax()],
                              np.average(bin_centers, weights=n),
                              SVMLabels.other.name))

    svm_train_data.append(n)
    svm_train_labels.append(SVMLabels.other.value)

    # DOWN Section
    down_img_split = split_image(img, 0.3, 0.7)
    down_shrink_img = split_image(img_shrink_clean, 0.3, 0.7)
    down_shrink_array = np.asarray(down_shrink_img).ravel()

    n, bins, bin_centers = plot_img_and_hist(down_img_split, down_shrink_array, fig, axis[2], axis[3])
    print(template_str.format(img_name,
                              'DOWN',
                              np.max(n),
                              n.argmax(),
                              bins[n.argmax()],
                              np.average(bin_centers, weights=n),
                              img_label.name))

    svm_train_data.append(n)
    svm_train_labels.append(img_label.value)


def extract_car_sample(index: int) -> Dict:
    svm_test_data = []
    svm_test_labels = []
    svm_train_data_pop = svm_train_data.copy()
    svm_train_labels_pop = svm_train_labels.copy()

    if index % 2 == 0:
        svm_test_data.append(svm_train_data[index])
        svm_test_labels.append(svm_train_labels[index])
        svm_train_data_pop.pop(index)
        svm_train_labels_pop.pop(index)

        svm_test_data.append(svm_train_data[index + 1])
        svm_test_labels.append(svm_train_labels[index + 1])
        svm_train_data_pop.pop(index + 1)
        svm_train_labels_pop.pop(index + 1)

    elif index % 2 == 1:
        svm_test_data.append(svm_train_data[index - 1])
        svm_test_labels.append(svm_train_labels[index - 1])
        svm_train_data_pop.pop(index - 1)
        svm_train_labels_pop.pop(index - 1)

        svm_test_data.append(svm_train_data[index])
        svm_test_labels.append(svm_train_labels[index])
        svm_train_data_pop.pop(index)
        svm_train_labels_pop.pop(index)

    svm_sample = {
        'train_data': svm_train_data_pop,
        'train_labels': svm_train_labels_pop,
        'test_data': svm_test_data,
        'test_labels': svm_test_labels
    }

    return svm_sample


def fit_and_predict_svm():
    # extract some test data
    random_index = np.random.randint(len(svm_train_data))
    print("Random index:", random_index)
    svm_sample = extract_car_sample(random_index)

    clf = svm.SVC(kernel='linear', gamma='scale')
    clf.fit(svm_sample['train_data'], svm_sample['train_labels'])

    # prediction
    prediction = clf.predict(svm_sample['test_data'])

    print("TOP: Truth label: {}\tPredicted label: {}".format(
        SVMLabels(svm_sample['test_labels'][0]).name,
        SVMLabels(prediction[0]).name))

    print("BOTTOM: Truth label: {}\tPredicted label: {}".format(
        SVMLabels(svm_sample['test_labels'][1]).name,
        SVMLabels(prediction[1]).name))


def test_with_all_patch() -> SVMLabel_type:
    predicted_label = []
    for test_index, test_data in enumerate(svm_train_data, 0):
        test_label = svm_train_labels[test_index]

        svm_train_data_pop = svm_train_data.copy()
        svm_train_labels_pop = svm_train_labels.copy()
        svm_train_data_pop.pop(test_index)
        svm_train_labels_pop.pop(test_index)

        clf = svm.SVC(kernel='linear', gamma='scale')
        clf.fit(svm_train_data_pop, svm_train_labels_pop)

        # prediction
        prediction = clf.predict([test_data])
        predicted_label.append(SVMLabels(prediction[0]).value)

        template_str = "Testing index {:2d}\tTruth {:10}\tPredicted {:10}"
        if SVMLabels(test_label).name == SVMLabels(prediction[0]).name:
            template_str += "\tCorrect!"

        print(template_str.format(test_index, SVMLabels(test_label).name, SVMLabels(prediction[0]).name))

        if test_index == 13:
            print(100 * "-")

    return predicted_label


# load images
positive_img, negative_img = load_images()

# positive processing
fig_pos, axs_pos = plt.subplots(len(positive_img) * 2, 2)
fig_pos.subplots_adjust(hspace=.4, wspace=0)
fig_pos.set_figheight(25)

print("Processing POSITIVE patch")
for idx, image_path in enumerate(positive_img, 0):
    axis_list = [axs_pos[idx * 2, 0], axs_pos[idx * 2, 1],
                 axs_pos[(idx * 2) + 1, 0], axs_pos[(idx * 2) + 1, 1]]

    processing_image(image_path, SVMLabels.hot_engine, fig_pos, axis_list)

    axis_list[0].set_ylabel(str(idx * 2))
    axis_list[2].set_ylabel(str(idx * 2 + 1))

# negative processing
fig_pos, axs_pos = plt.subplots(len(negative_img) * 2, 2)
fig_pos.subplots_adjust(hspace=.4, wspace=0)
fig_pos.set_figheight(25)

indexing_offset = len(positive_img * 2)

print("Processing NEGATIVE patch")
for idx, image_path in enumerate(negative_img, 0):
    axis_list = [axs_pos[idx * 2, 0], axs_pos[idx * 2, 1],
                 axs_pos[(idx * 2) + 1, 0], axs_pos[(idx * 2) + 1, 1]]

    processing_image(image_path, SVMLabels.other, fig_pos, axis_list)

    axis_list[0].set_ylabel(str(idx * 2 + indexing_offset))
    axis_list[2].set_ylabel(str(idx * 2 + 1 + indexing_offset))

# SVM
fit_and_predict_svm()

predicted_labels = test_with_all_patch()
target_names = ['other', 'hot engine']

print(classification_report(svm_train_labels, predicted_labels, target_names=target_names))
print(confusion_matrix(svm_train_labels, svm_train_labels, labels=range(len(target_names))))
