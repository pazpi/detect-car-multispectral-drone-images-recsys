import os
import cv2

directory_img = 'frame-blur/'


def variance_of_laplacian(image):
    # compute the Laplacian of the image and then return the focus
    # measure, which is simply the variance of the Laplacian
    return cv2.Laplacian(image, cv2.CV_64F).var()


# get all file into a directory
list_img_name = []
for (dirpath, dirnames, filenames) in os.walk(directory_img):
    list_img_name.extend(filenames)

for img_path in list_img_name:
    img = cv2.imread(directory_img + img_path)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur_level = variance_of_laplacian(gray_img)
    cv2.putText(img, "{}: {:.2f}".format('Laplacian blur: ', blur_level), (10, 30),
                cv2.FONT_HERSHEY_SIMPLEX, 0.8, (0, 0, 255), 3)
    cv2.imshow("Image", img)
    key = cv2.waitKey(0)
    print('Image: {0} blur: {1:20}'.format(img_path, blur_level))


# if cv2.waitKey(1) & 0xFF == ord('q'):
#     break
