import os
import cv2
import numpy as np
from matplotlib import pyplot as plt


top_dir = './frame-ir'

positive_img = []
negative_img = []

for root, dirs, files in os.walk(top_dir, topdown=True):
    for name in files:
        if root == os.path.join(top_dir, 'positive'):
            positive_img.append(os.path.join(root, name))
        if root == os.path.join(top_dir, 'negative'):
            negative_img.append(os.path.join(root, name))

for image in positive_img:
    img = cv2.imread(image, 0)
    img = cv2.equalizeHist(img)
    # Initiate STAR detector
    orb = cv2.ORB_create()

    # find the keypoints with ORB
    kp = orb.detect(img, None)

    # compute the descriptors with ORB
    kp, des = orb.compute(img, kp)

    # draw only keypoints location,not size and orientation
    img2 = cv2.drawKeypoints(img, kp, np.array([]), color=(0, 255, 0), flags=0)
    # res = np.hstack((img, img2))
    plt.imshow(img2), plt.show()
