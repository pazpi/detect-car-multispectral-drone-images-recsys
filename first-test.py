import cv2
import time
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import logging.handlers

# enable logging
f = logging.Formatter(fmt='%(levelname)s:%(name)s: %(message)s (%(asctime)s; %(filename)s:%(lineno)d)',
                      datefmt="%Y-%m-%d %H:%M:%S")
handlers = [
    logging.handlers.RotatingFileHandler('root.log', encoding='utf8', maxBytes=100000, backupCount=1),
    logging.StreamHandler()
]
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
for h in handlers:
    h.setFormatter(f)
    h.setLevel(logging.DEBUG)
    log.addHandler(h)


list_video = {'Visibile': 'Multisensor-Visible.avi',
              'Thermal': 'Multisensor-Thermal.avi'}

# needed for video sync
thermal_skip_frame = 24

v_visibile = cv2.VideoCapture(list_video['Visibile'])
v_thermal = cv2.VideoCapture(list_video['Thermal'])


def skip_frame(video, frame_to_skip):
    curr_frame = 0
    while video.isOpened():
        ret, frame = video.read()
        if ret:
            curr_frame += 1
            if curr_frame < frame_to_skip:
                pass
            else:
                return
        else:
            break


def main():
    # sync visible and thermal video
    skip_frame(v_thermal, thermal_skip_frame)

    # Create figure and axes
    plt.ion()
    fig = plt.figure(dpi=300)
    plt_v = fig.add_subplot(131)
    plt_v.axis('off')
    plt_t = fig.add_subplot(132)
    plt_t.axis('off')
    plt_crop_v = fig.add_subplot(133)
    plt_crop_v.axis('off')

    crop_visible_x = 137
    crop_visible_dx = 277 - crop_visible_x
    crop_visible_y = 215
    crop_visible_dy = 416 - crop_visible_y

    while v_visibile.isOpened() and v_thermal.isOpened():
        ret_v, frame_v = v_visibile.read()
        ret_t, frame_t = v_thermal.read()

        frame_t = cv2.flip(frame_t, 0)
        frame_t = cv2.flip(frame_t, 1)

        # frame_crop_v = frame_v[137:283, 215:416]
        frame_crop_v = frame_v[crop_visible_x:crop_visible_x+crop_visible_dx,
                               crop_visible_y:crop_visible_y+crop_visible_dy]

        # thermal_resize = cv2.resize(frame_t[1], (100, 100))
        # cv2.imshow("Resize thermal", thermal_resize)

        plt_v.imshow(frame_v)
        plt_t.imshow(frame_t)
        plt_crop_v.imshow(frame_crop_v)
        # Create a Rectangle patch
        rect = patches.Rectangle((crop_visible_x + crop_visible_dx/2, crop_visible_y + crop_visible_dy/2),
                                 crop_visible_dy, -crop_visible_dx, linewidth=1, edgecolor='r', facecolor='none')

        # Add the patch to the Axes
        plt_v.add_patch(rect)

        fig.canvas.draw()

        time.sleep(3)
        # cv2.imshow("Image Visible", frame_v)
        # cv2.imshow("Image Visible Crop", frame_crop_v)
        # cv2.imshow("Image Thermal", frame_t)
        # key = cv2.waitKey(0)


if __name__ == "__main__":
    main()
