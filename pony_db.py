from pony.orm import *

_db = Database()


class Frame(_db.Entity):
    id = PrimaryKey(int, auto=True)
    frame = Required(int, unique=True)
    laplacian_blur = Required(float)


_params = dict(
    sqlite=dict(provider='sqlite', filename='frame_database.sqlite', create_db=True),
    mysql=dict(provider='mysql', host="localhost", user="pony", passwd="pony", db="pony"),
    postgres=dict(provider='postgres', user='pony', password='pony', host='localhost', database='pony'),
    oracle=dict(provider='oracle', user='c##pony', password='pony', dsn='localhost/orcl')
)

_db.bind(**_params['sqlite'])

_db.generate_mapping(create_tables=True)
